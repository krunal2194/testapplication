import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from '../common.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {
  first_name = '';
  last_name = '';
  email = '';
  password = '';
  constructor(private route: Router, private common: CommonService) { }

  ngOnInit() {
  }

  signup(){
    if(this.first_name != '' && this.last_name != '' && this.email != '' && this.password != '' && this.common.validateEmail(this.email)){
      this.route.navigate(['/listing']);
    }
    else {
      this.common.presentToast('Please enter all your details', 'bottom', 2100);
    }
  }

}
