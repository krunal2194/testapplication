import { Component, OnInit, Input } from '@angular/core';
import { Product } from '../data.service';
import { CommonService } from '../common.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-productlist',
  templateUrl: './productlist.page.html',
  styleUrls: ['./productlist.page.scss'],
  inputs: ['recieved_data']
})
export class ProductlistPage implements OnInit {

  @Input() recieved_data: Array<Product>;

  constructor(private fun: CommonService, private nav: NavController) {
  }

  ngOnInit() {
    console.log(this.recieved_data)
  }

  open(data){
    this.fun.update(data);
    this.nav.navigateForward('/productdetail');
  }

}

