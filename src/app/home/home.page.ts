
import { Component, ViewChild } from '@angular/core';
import { MenuController, IonSlides } from '@ionic/angular';
// import { DataService, HomeTab, Product } from '../data.service';
import { ActivatedRoute, Router } from '@angular/router';
import { CommonService } from '../common.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  slideOpts = {
    initialSlide: 1,
    speed: 400
  };
  constructor(
    private activatedRoute: ActivatedRoute,
    private menuCtrl: MenuController,
    private common: CommonService,
   private route: Router) {
   
   }

   ionViewDidEnter() {
    this.menuCtrl.enable(true, 'start');
    this.menuCtrl.enable(true, 'end');
  }

  listingPage() {
    this.route.navigate(['/listing']);
  }
  
}
