import { Component, OnInit } from '@angular/core';
import { Platform } from '@ionic/angular';
import { Router } from '@angular/router';
import { CommonService } from '../common.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  email = '';
  password = '';

  constructor(private platform: Platform, private route: Router, private common:  CommonService) { }

  ngOnInit() {
  }

  signin() {
    this.platform.ready().then(() => {
        if (this.common.validateEmail(this.email) && this.password !== '') {
        this.route.navigate(['/home']);
        } else {
          this.common.presentToast('Please enter valid credentials', 'bottom', 2100);
        } 
    });
  }



  signUp() {
    this.route.navigate(['/signup']);
  }

}
