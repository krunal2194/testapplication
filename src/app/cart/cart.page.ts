import { Component, OnInit, ViewChild } from '@angular/core';
import { Cart, DataService } from '../data.service';
import { ModalController, IonList, NavController, MenuController, AlertController } from '@ionic/angular';
import { CommonService } from '../common.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.page.html',
  styleUrls: ['./cart.page.scss'],
})
export class CartPage implements OnInit {

  @ViewChild('slidingList') slidingList: IonList;

  customAlertOptions: any = {
    header: 'Select Quantity',
    translucent: true
  };

  qty = [];
  code = '';
  show = true;
  data: Array<Cart> = [];

  constructor(
    private menuCtrl: MenuController,
    public dataService: DataService,
    public fun: CommonService,
    private modalController: ModalController,
    private nav: NavController,
    public alertController: AlertController) {
    this.data = dataService.cart;
    if (this.data.length === 0) { this.show = false; }
    for (let i = 1; i <= 36; i++) { this.qty.push(i); }
  }

  ngOnInit() {
  }





  calculate(i) {
    let res = 0;
    if (i === 0) {
      for (const j of this.data) {
        if (j.product.offer) {
          res += this.fun.calculate(j.product.cost_price, j.product.discount) * j.quantity;
        } else {
          res += j.product.cost_price * j.quantity;
        }
      }
    }
    if (i === 1) {
      for (const j of this.data) {
        res += j.product.shipping;
      }
    }
    return res;
  }


  fix(a) {
    return a.toFixed(2);
  }

  add() {
    const res = this.calculate(1) + this.calculate(0);
    return res;
  }

  browse() {
    this.fun.navigate('/home', false);
  }



}
